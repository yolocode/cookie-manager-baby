# Cookie Manager Baby !!!

Install the library in your html page

```html
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>My beautiful website</title>
            <script src="https://gitlab.com/yolocode/cookie-manager-baby/-/raw/master/dist/bundle.js"></script>
        </head>
```

Create the html according to your needs

```html
<body>
    <div id="askElement" style="display: none">
        <button id="acceptAll">Accept</button>
        <button id="rejectAll">Refuse</button>
        <div>
            <div>
                <label for="hotjar-acceptance">Hotjar</label>
                <input type="checkbox" id="hotjar-acceptance">
            </div>
            <div>
                <label for="gtag-acceptance">google tag</label>
                <input type="checkbox" id="gtag-acceptance">
            </div>
            <button id="savePreferences">Save preferences</button>
        </div>
    </div>
</body>
```

Use cookie manager at the end of your html file

```html
    <script>
        const config = {
            // The id of the container that ask for cookies
            cookieManagerHtmlElement: "askElement",
            // The id of the "accept all" html element
            acceptAll: "acceptAll",
            // The id of the "reject all" html element
            rejectAll: "rejectAll",
            // The id of the save preferences button
            saveConfig: "savePreferences",
            //The object containing all the trackers
            trackers: {
                gtag: {
                    // The id of the checkbox wich determines weither or not the tracker is enabled 
                    id: 'gtag-acceptance',
                    // The function executed when the tracker is enabled
                    activator: () => {
                        console.log('gtag autorisé')
                    },
                    // The function executed when the tracker is not enabled
                    deactivator: () => {
                        console.log('gtag non nautorisé')
                    }
                },
                hotjar: {
                    id: 'hotjar-acceptance',
                    activator: () => {
                        console.log('hotjar autorisé')
                    },
                    deactivator: () => {
                        console.log('hotjar non nautorisé')
                    }
                }
            }
        }

        // Launch cookie manager
        cmBaby.CookieManager(config)
    </script>
```

The values are stored in cookies. This way, the user won't have to answer wheither or not he wants cookies everytime he uses your website
