import { getCookie, setCookie, getTrackersRegisteredStates, resetCookieManager } from './cookieManager'
import { Tracker } from './tracker'

class TrackersManager{
  constructor(cookieManagerHtmlElement, acceptanceId, rejectanceId, preferencesId, trackers) {
    this.cookieManagerHtmlElement = cookieManagerHtmlElement
    this.acceptanceId = acceptanceId
    this.rejectanceId = rejectanceId
    this.preferencesId = preferencesId
    this.trackers = trackers

    // If the trackersManager is not properly registered yet
    if (!this.checkAlreadyExistingCookies()) {
      this.printHtmlElementToAskForCookies()
      resetCookieManager()
      this.initTrackerWaitingForAcceptanceOrRejectance()
      this.initWaitingForPreferences()
    } else {
      this.activateAll()
    }
    
  }

  printHtmlElementToAskForCookies() {
    const element = document.getElementById(this.cookieManagerHtmlElement)
    if (element) {
      element.removeAttribute("style")
    }
  }

  hideHtmlElementForCookies() {
    const element = document.getElementById(this.cookieManagerHtmlElement)
    if (element) {
      element.setAttribute("style", "display: none;")
    }
  }

  createJSONForTrackers() {
    var jsonStates = {}
    for (let tracker of this.trackers) {
      jsonStates[tracker.id] = tracker.activated
    }
    return JSON.stringify(jsonStates)
  }

  /**
   * This function is designed to say if the cookies stored 
   * in the user's browser are the same that are in the configuration
   * Even if there is only one mismatch, we say that the already existing cookie is
   * not correct. We then decide to make as if there is no existing cookie 
   * for cookie manager and reset eveything.
   */
  checkAlreadyExistingCookies() {
    let trackersState = getTrackersRegisteredStates()
    if (trackersState) {
      for (let tracker of this.trackers) {
        if (!trackersState.hasOwnProperty(tracker.id)) {
          return false
        }
      }
      return true
    }
    return false
  }

  initTrackerWaitingForAcceptanceOrRejectance() {
    let elementAccept = document.getElementById(this.acceptanceId)
    if(elementAccept) {
      elementAccept.addEventListener('click', () => {
        this.acceptAll()
      })
    }

    let elementReject = document.getElementById(this.rejectanceId)
    if(elementReject) {
      elementReject.addEventListener('click', () => {
        this.rejectAll()
      })
    }
  }

  initWaitingForPreferences() {
    if (this.preferencesId) {
      let preferencesElement = document.getElementById(this.preferencesId)
      if(preferencesElement) {
        preferencesElement.addEventListener('click', () => {
          this.savePreferences()
        })
      }
    }
  }

  savePreferences() {
    for (let tracker of this.trackers) {
      const acceptTrackerElt = document.getElementById(tracker.htmlId)
      if (acceptTrackerElt) {
        if (acceptTrackerElt.checked) {
          tracker.activate()
        } else {
          tracker.deactivate()
        }
      }
    }
    setCookie("cookie-manager", this.createJSONForTrackers(), 365)
    this.hideHtmlElementForCookies()
  }

  activateAll() {
    for (let tracker of this.trackers) {
      tracker.activate()      
    }
  }

  deactivateAll() {
    for (let tracker of this.trackers) {
      tracker.deactivate()
    }
  }

  acceptAll() {
    this.activateAll()
    setCookie("cookie-manager", this.createJSONForTrackers(), 365)
    this.hideHtmlElementForCookies()
  }

  rejectAll() {
    this.deactivateAll()
    setCookie("cookie-manager", this.createJSONForTrackers(), 365)
    this.hideHtmlElementForCookies()
  }
}
const CookieManager = function(config) {
  let array_trackers = []
  for(const [key, value] of Object.entries(config.trackers)) {
      array_trackers.push(new Tracker(key, value.id, value.activator, value.deactivator))
  }

  return new TrackersManager(config.cookieManagerHtmlElement, config.acceptAll, config.rejectAll, config.saveConfig || null, array_trackers)
}

export { CookieManager }