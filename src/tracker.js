import { getCookie, setCookie } from './cookieManager'

export class Tracker{
  constructor (id, htmlId, activator, deactivator) {
    this.id = id
    this.htmlId = htmlId
    this.activator = activator
    this.deactivator = deactivator
    this.name = `cookie-manager-${id}`
    this.activated = false
  }

  activate() {
    this.activated = true
    this.activator()
  }

  deactivate() {
    this.activated = false
    this.deactivator()
  }

}