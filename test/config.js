export const config = {
  cookieManagerHtmlElement: "askElement",
  acceptAll: "acceptAll",
  rejectAll: "rejectAll",
  saveConfig: "saveConfig",
  trackers: {
    gtag: {
      id: 'gtag-acceptance',
      activator: () => {
        return 'gtag activated'
      },
      deactivator: () => {
        return 'gtag activated'
      }
    },
    hotjar: {
      id: 'hotjar-acceptance',
      activator: () => {
        return 'gtag'
      },
      deactivator: () => {
        return 'hotjar activated'
      }
    }
  }
}