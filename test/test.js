import { getCookie, setCookie, unsetCookie } from '../src/cookieManager'
import { Tracker} from '../src/tracker'
import { CookieManager } from '../src/index'
import { config } from './config'


test('Cookie setter and getter', () => {
  setCookie("test", "essai", 365)
  expect(getCookie("test")).toBe("essai")

  unsetCookie("test")
  expect(getCookie("test")).toBe("")
});

test('Tracker', () => {
  let tracker = new Tracker("gtag", "gtag-radio-button", () => "activated", () => "deactivated" )
  expect(tracker.id).toEqual("gtag")
  expect(tracker.activator()).toEqual("activated")
  expect(tracker.name).toBe("cookie-manager-gtag")

  tracker.activate()
  expect(tracker.activated).toBe(true)

  tracker.deactivate()
  expect(tracker.activated).toBe(false);
})

test('Functionnal', () => {
  let cookieManager = CookieManager(config)

  expect(cookieManager.trackers.length).toBe(2)

  const tracker = cookieManager.trackers[0]
  expect(tracker.id).toEqual("gtag")
  expect(tracker.activator()).toEqual("gtag activated")
  expect(tracker.name).toBe("cookie-manager-gtag")
})

const html1 = `
<div id="askElement" style="display:none;">
  <button id="acceptAll">Accepter</button> 
  <button id="rejectAll">Refuser</button>
  <input type="checkbox" id="gtag-acceptance"/>
  <input type="checkbox" id="hotjar-acceptance"/>
  <button id="saveConfig">Sauvegarder</button>
</div>`

test('Accept all', () => {
  unsetCookie("cookie-manager")
  // Set up our document body
  document.body.innerHTML = html1
  const askElement = document.getElementById("askElement")
  
  let cookieManager = CookieManager(config)
  expect(getCookie("cookie-manager")).toBe("")
  document.getElementById('acceptAll').click()
  expect(getCookie("cookie-manager")).toEqual('{"gtag":true,"hotjar":true}')
  expect(askElement.getAttribute("style")).toBe("display: none;")

  askElement.removeAttribute("style")
  expect(askElement.getAttribute("style")).toBe(null)
  document.getElementById('rejectAll').click()
  expect(getCookie("cookie-manager")).toEqual('{"gtag":false,"hotjar":false}')
  expect(askElement.getAttribute("style")).toBe("display: none;")
});

test('Accept partially', () => {
  unsetCookie("cookie-manager")
  // Set up our document body
  document.body.innerHTML = html1
  CookieManager(config)
  expect(getCookie("cookie-manager")).toEqual("")

  document.getElementById('hotjar-acceptance').checked = true
  document.getElementById('gtag-acceptance').checked = false

  document.getElementById('saveConfig').click()
  expect(getCookie("cookie-manager")).toEqual('{"gtag":false,"hotjar":true}')
})

const html2 = `
<div id="askElement" style="display:none;">
  <button id="acceptAll">Accepter</button> 
  <button id="rejectAll">Refuser</button>
  <input type="checkbox" id="gtag-acceptance"/>
  <input type="checkbox" id="hotjar-acceptance"/>
  <button id="saveConfig">Sauvegarder</button>
</div>`
  
test('Reject all not mandatory', () => {
  unsetCookie("cookie-manager")
  // Set up our document body
  document.body.innerHTML = html2
  CookieManager(config)
  expect(document.getElementById("askElement").getAttribute("style")).toBe(null)
  expect(getCookie("cookie-manager")).toBe("")

  document.getElementById('hotjar-acceptance').checked = true
  document.getElementById('gtag-acceptance').checked = false

  document.getElementById('saveConfig').click()
  expect(getCookie("cookie-manager")).toBe('{"gtag":false,"hotjar":true}')
})

const html3 = `
<div id="askElement" style="display:none;">
  <button id="acceptAll">Accepter</button> 
  <button id="rejectAll">Refuser</button>
</div>`

test('Save config not defined', () => {
  unsetCookie("cookie-manager")
  // Set up our document body
  document.body.innerHTML = html3
  delete config.saveConfig
  CookieManager(config)

  document.getElementById('acceptAll').click()
  expect(getCookie("cookie-manager")).toBe('{"gtag":true,"hotjar":true}')
})
